# ReduxSimpleStarter

### Getting Started
Checkout this repo, install dependencies, then start the webpack dev server:

```
> git clone git@bitbucket.org:adearlife/rock7techtest.git
> cd rock7techtest
> npm install
> npm start
```

### Project Idea

As a quick idea i thought i would try and create a simple D3 map. It could show all upcoming active or recent races. Currently it is only showing the three mentioned on the github page.
You can then click on a marker and get more information about that race. You could extend this to then have an option to move onto the map viewer to get a more detailed view of each race.
You could also extend this so that it automatically cycles if someone does not click for a period of time.

I have made a lot of assumptions with the data being returned so some items of data may be incorrect.

FYI: Your endpoints returned 503’s on the first time hitting them. If this happens the app will just show a blank map. It can take a few seconds for the data to load.

