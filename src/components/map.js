import React, {Component} from "react";
import {
    ComposableMap,
    ZoomableGroup,
    Geographies,
    Geography,
    Markers,
    Marker,
} from "react-simple-maps";
import {Motion, spring} from "react-motion";
import PropTypes from 'prop-types';

class Map extends Component {
    constructor() {
        super();
        this.state = {
            center: [0, 20],
            zoom: 1,
        };
        this.handleMarkerClick = this.handleMarkerClick.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    convertDataToRequiredCoordinateFormat() {
        return this.props.R7Data.map((race) => {
            return {
                coordinates: [race.startingLocation.lon, race.startingLocation.lat],
                eventName: race.eventName
            }
        })
    }

    handleMarkerClick(marker) {
        this.props.selectActiveEvent(marker.eventName);
        this.setState({
            zoom: 2,
            center: marker.coordinates,
        })
    }

    handleReset() {
        this.props.removeActiveEvent();
        this.setState({
            center: [0, 20],
            zoom: 1,
        })
    }

    render() {
        return (
            <div>
                {this.props.activeEvent && <button className='resetButton' onClick={this.handleReset}>Reset</button>}
                <Motion
                    defaultStyle={{
                        zoom: 1,
                        x: 0,
                        y: 20,
                    }}
                    style={{
                        zoom: spring(this.state.zoom, {stiffness: 60, damping: 20}),
                        x: spring(this.state.center[0], {stiffness: 60, damping: 20}),
                        y: spring(this.state.center[1], {stiffness: 60, damping: 20}),
                    }}
                >
                    {({zoom, x, y}) => (
                        <ComposableMap
                            style={{
                                width: "100%",
                                height: "auto",
                            }}
                        >
                            <ZoomableGroup center={[x, y]} zoom={zoom}>
                                <Geographies
                                    geography="https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/examples/with-react-motion/static/world-110m.json">
                                    {(geographies, projection) =>
                                        geographies.map((geography, i) => geography.id !== "010" && (
                                            <Geography
                                                key={i}
                                                geography={geography}
                                                projection={projection}
                                                style={{
                                                    default: {
                                                        fill: "#ECEFF1",
                                                        stroke: "#607D8B",
                                                        strokeWidth: 0.75,
                                                        outline: "none",
                                                    },
                                                    hover: {
                                                        fill: "#ECEFF1",
                                                        stroke: "#607D8B",
                                                        strokeWidth: 0.75,
                                                        outline: "none",
                                                    },
                                                    pressed: {
                                                        fill: "#ECEFF1",
                                                        stroke: "#607D8B",
                                                        strokeWidth: 0.75,
                                                        outline: "none",
                                                    },
                                                }}
                                            />
                                        ))}
                                </Geographies>
                                <Markers>
                                    {this.convertDataToRequiredCoordinateFormat().map((city, i) => (
                                        <Marker
                                            key={i}
                                            marker={city}
                                            onClick={this.handleMarkerClick}
                                        >
                                            <circle
                                                cx={0}
                                                cy={0}
                                                r={6}
                                                fill="#268BAD"
                                                stroke="#1C6C86"
                                            />
                                        </Marker>
                                    ))}
                                </Markers>
                            </ZoomableGroup>
                        </ComposableMap>
                    )}
                </Motion>
            </div>
        )
    }
}

Map.propTypes = {
    R7Data: PropTypes.array,
    selectActiveEvent: PropTypes.func,
    removeActiveEvent: PropTypes.func,
    activeEvent: PropTypes.object
};

export default Map