import React, {Component} from 'react';
import R7 from '../util/R7';
import sToTime from '../util/sToTime';
import Map from './map';
import '../../style/style.scss';

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            R7Data: [],
            activeEvent: null
        };
        this.removeActiveEvent = this.removeActiveEvent.bind(this);
        this.selectActiveEvent = this.selectActiveEvent.bind(this);
    }

    componentDidMount() {
        const events = ['c6002016', 'arc2015', 'fastnet2015'];
        events.forEach((eventName) => {
            // for alot of this information there would be a a lot better ways of bringing this back in the binary in simpler ways.
            R7.load(eventName, (data) => {
                const R7Data = this.state.R7Data;
                // I am guessing the time is in seconds.
                const duration = data[0].times[0] - data[0].times[data[0].times.length - 1];
                R7Data.push({
                    teams: data.length,
                    startingLocation: {
                        // find the first recorded location.
                        lat: data[0].positions[data[0].positions.length - 1].lat,
                        lon: data[0].positions[data[0].positions.length - 1].lon,
                    },
                    duration: sToTime(duration),
                    eventName
                });
                this.setState({R7Data});
            });
        });
    }

    selectActiveEvent(eventName) {
        const activeEvent = this.state.R7Data.find((event) => {
            return eventName === event.eventName;
        });
        this.setState({
            activeEvent
        })
    }

    removeActiveEvent() {
        this.setState({
            activeEvent: null
        })
    }

    render() {
        return (
            <div>
                <Map
                    R7Data={this.state.R7Data}
                    selectActiveEvent={this.selectActiveEvent}
                    activeEvent={this.state.activeEvent}
                    removeActiveEvent={this.removeActiveEvent}
                />
                <div className='EventDetails'>
                    {this.state.activeEvent && <div>
                        <h3>{this.state.activeEvent.eventName}</h3>
                        <p>Amount of teams: {this.state.activeEvent.teams}</p>
                        <p>Race duration: {this.state.activeEvent.duration}</p>
                    </div>}
                    {!this.state.activeEvent && <div>
                        <p>Please select a marker of an event to view more information</p>
                    </div>}
                </div>
            </div>
        );
    }
}
